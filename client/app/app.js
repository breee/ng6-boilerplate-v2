import angular          from 'angular'
import ngAnimate        from 'angular-animate'
import ngAria           from 'angular-aria'
import ngMessages       from 'angular-messages'
import ngRoute          from 'angular-route'
import uiRouter         from 'angular-ui-router'
import ngMaterial       from 'angular-material'

import Common           from './common/common'
import Components       from './components/components'
import AppComponent     from './app.component'

import { AuthService,
         AuthInterceptorFactory,
         ConfigService,
         LoginService } from './common/auth/services'
// import LoginCtrl        from './common/auth/login/login.controller'
import 'angular-ui-router/release/stateEvents'

import 'normalize.css'

angular.module('app', [
    uiRouter,
    'ui.router.state.events',
    ngMaterial,
    Common,
    Components
  ])
  .service('AuthService', AuthService)
  .service('LoginService', LoginService)
  .factory('AuthInterceptor', AuthInterceptorFactory)
  .value('ConfigService', ConfigService)
  .config(($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) => {
    "ngInject"
    $httpProvider.interceptors.push('AuthInterceptor')
    $locationProvider.html5Mode(true)
  })
  .run(function($rootScope, $state, $window, AuthService) {
    "ngInject"
    $rootScope.$on("$stateChangeStart", function(event, nextRoute, currentRoute) {
        if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication
          && !AuthService.isAuthenticated() && !$window.localStorage.token) {
          event.preventDefault()
          $state.transitionTo('login')
      }
    $rootScope.$state = $state
    })
  })
  .run(function($rootScope, $state, $stateParams) {
    "ngInject"
    $rootScope.$on('$stateChangeStart',
      (event, toState  , toParams , fromState, fromParams) => {
    })
  })

  .component('app', AppComponent)
