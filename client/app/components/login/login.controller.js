class LoginController {
  constructor ($state, LoginService) {
    this.$state = $state;
    this.loginService = LoginService;

    this.loginInProgress = false;
  }

  login () {
    this.loginInProgress = true;
    this.loginService.login(this.email, this.password).then(
      () => {
        this.loginInProgress = false;
        this.$state.go('home');
      },
      (error) => {
        this.loginInProgress = false
      }
    )
  }
}

LoginController.$inject = ['$state', 'LoginService'];

export default LoginController;
