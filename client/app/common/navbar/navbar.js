import angular         from 'angular';
import uiRouter        from 'angular-ui-router';
import navbarComponent from './navbar.component';

import 'angular-material/angular-material.min.css'

let navbarModule = angular.module('navbar', [
  uiRouter
])

.component('navbar', navbarComponent)

.name;

export default navbarModule;
