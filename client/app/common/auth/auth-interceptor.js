// Workaround for not losing the this binding in methods
let self;

class AuthInterceptor {
  constructor ($state, $location, $q, ConfigService, AuthService) {
    this.$state = $state;
    this.$location = $location;
    this.$q = $q;
    this.configService = ConfigService;
    this.authService = AuthService;
    this.API_LOGIN_URL = `${ConfigService.base_url}/login/`;

    self = this;
  }

  request (config) {
    var canceller;
    if (config.url.search(self.configService.apiBase) !== -1 &&
        self.authService.isAuthenticated()) {
      config.headers['x-jwt-token'] = self.authService.getCredentials().token;
    } else if (config.url.search(self.configService.apiBase) !== -1 &&
        config.url !== self.API_LOGIN_URL) {
      canceller = self.$q.defer();
      config.timeout = canceller.promise;
      canceller.resolve(`Cancelled request to ${config.url} because we do not have credentials`);
      self.authService.cleanCredentials();
      self.$state.transistionTo('/login');
    }
    return config;
  }

  responseError (rejection) {
    if (rejection.config.url.search(self.configService.apiBase) !== -1 &&
        rejection.status === 401) {
      // TODO: redirect parameter for login
      self.authService.cleanCredentials();
      self.$state.transistionTo('/login');
    }
    return self.$q.reject(rejection);
  }


  static factory ($state, $location, $q, ConfigService, AuthService) {
    return new AuthInterceptor($state, $location, $q, ConfigService, AuthService);
  }
}

AuthInterceptor.factory.$inject = ['$state', '$location', '$q','ConfigService', 'AuthService'];

export default AuthInterceptor.factory;
