class LoginService {
  constructor ($http, $q, $state, ConfigService, AuthService) {
    this.$http = $http;
    this.$q = $q;
    this.$state = $state;
    this.configService = ConfigService;
    this.authService = AuthService;

    if (AuthService.isAuthenticated() && $state.current.name === 'login') {
      this.$state.go('home');
    }

    if (!AuthService.isAuthenticated() && $state.current.name !== 'login') {
      this.$state.go('login');
    }
  }

  login (username, password) {
    /*jshint camelcase: false */
    this.authService.cleanCredentials();
    return this.$http.post(`${this.configService.apiBase}/login/`, {
      email: username,
      password: password
    })
      .success(data => {
        this.authService.setCredentials(data.token);
        this.signalsService.emit('renooit:login', username);
      })
      .catch(error => this.$q.reject(error.data.non_field_errors[0]));
  }

  logout () {
    return this.$http.get(`${this.configService.apiBase}/logout/`)
      .success(() => {
        this.authService.cleanCredentials();
        this.$state.go('login');
      })
      .catch(error => this.$q.reject(error.data));
  }
}

LoginService.$inject = [
  '$http', '$q', '$state', 'ConfigService', 'AuthService'
];

export default LoginService;
